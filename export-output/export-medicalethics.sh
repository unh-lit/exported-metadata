#!/usr/bin/bash

# /cygdrive/d/FedoraStorage/collections/acworth
# /Volumes/FedoraStorage/collections/acworth
# ./tmp/collections/acworth
#
# ./export-medicalEthics.sh > output-medicalEthics.txt 2>&1 &
# ./export-medicalEthics.sh >> output-medicalEthics.txt 2>&1 &
# tail -f output-medicalEthics.txt

./f3export.sh -q "pid~medicalEthics:001" -f -d "/cygdrive/d/FedoraStorage/collections/medicalEthics" -o "medicalEthics.csv"
./f3export.sh -q "pid~medicalEthics:002" -f -d "/cygdrive/d/FedoraStorage/collections/medicalEthics" -o "medicalEthics.csv" -a
./f3export.sh -q "pid~medicalEthics:003" -f -d "/cygdrive/d/FedoraStorage/collections/medicalEthics" -o "medicalEthics.csv" -a